Queue - a list of people to be added
====================================
Note that this list is subject to change. If you know an anarchist you want added to this list, or someone you see who does not belong here, please make an issue for it. I want a wide range of anarchists, including those who disagree with each other and are in direct conflict with each other.

Because the French anarchist fortune has a disproportionate number of French thinkers and artists, I am trying to balance the ratio with Spanish, Italian, American, English, German, Eastern European, and Russian anarchists. I am also going to look for anarchistic thinking in East Asia, the Middle East, and Africa.

The miscellaneous file allows for non-anarchists who had some ideas which are in the spirit of anarchism, for example Bertrand Russell and, er, J. R. R. Tolkien. However, explicit anarchists get priority.

1.9.0 - syndicalist edition
---
- ~~James Guillaume~~
- ~~Ricardo Mella~~
- ~~Fernand Pelloutier~~
- ~~Rudolf Rocker~~
- ~~Guy Aldred~~
- ~~Angel Pestana~~
- ~~Gregori Maximoff~~
- ~~Buenaventura Durruti~~
- ~~Abel Paz~~
- ~~Lorenzo Kom'boa Ervin~~

For later:
----------
- Ursula LeGuin
- Octavia Butler
- Tom Morello
- L. Susan Brown
- Dave Andrews
- Herbert Read
- Vermin Supreme
- Maria Lacerda de Moura
- Francisco Ferrer
- Luigi Galleani
- Lucia Sanchez Saornil
- Hakim Bey
- Elbert Hubbard
- Maria Nikiforova
- Voline
- Henry Appleton
- Adolph Fischer
- Ferdinand Domela Nieuwenhuis
- Yakoub Islam
- Ivan Illich
- Shawn P. Wilbur
- Zaher Baher
- Victor Serge
- Randolph Bourne

Other anarchist resources:
--------------------------
- libcom.org
- CrimethInc.
- Invisible Committee
- C4SS

To add to a misc. file:
-----------------------
- Zeno of Citium
- Laozi
- Rabindranath Tagore
- Mohandas Gandhi
- gmilcomic



