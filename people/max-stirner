Whoever will be free must make himself free. Freedom is no fairy gift to fall into a man's lap.
		-- Max Stirner
%
What is freedom? To have the will to be responsible for one's self.
		-- Max Stirner
%
The state calls its own violence law, but that of the individual, crime.
		-- Max Stirner
%
Do truth, freedom, humanity, justice, desire anything else than that you grow enthusiastic and serve them?
		-- Max Stirner
%
The divine is God's concern; the human, man's. My concern is neither the divine nor the human, not the true, good, just, free, etc., but solely what is mine.
		-- Max Stirner
%
The only thing I am not entitled to is what I do not do with a free cheer, that is, that I do not entitle myself to.
		-- Max Stirner
%
Every state is a despotism.
		-- Max Stirner
%
Freedom can only be the whole of freedom; a piece of freedom is not freedom.
		-- Max Stirner
%
The poor are to blame for there being rich men.
		-- Max Stirner
%
He who has might has right... is this wisdom so hard to attain?
		-- Max Stirner
%
The Revolution aimed at new arrangements; insurrection leads us no longer to let ourselves be arranged, but to arrange ourselves.
		-- Max Stirner
%
What you have the power to be you have the right to be.
		-- Max Stirner
%
The state always has the sole purpose to limit, tame, subordinate, the individual -- to make him subject to some generality or other.
		-- Max Stirner
%
God and mankind have concerned themselves for nothing, for nothing but themselves. Let me then likewise concern myself for myself, who am equally with God the nothing of all others, who am my all, who am the only one.
		-- Max Stirner
%
To be looked upon as a mere part, part of society, the individual cannot bear -- because he is more; his uniqueness puts from it this limited conception.
		-- Max Stirner
%
Society, from which we have everything, is a new master, a new spook, a new "supreme being," which "takes us into its service and allegiance!"
		-- Max Stirner
%
It is not another state that men aim at, but their union, uniting, this ever-fluid uniting of everything standing.
		-- Max Stirner
%
The state behaves as the same ruler that the church was. The latter rests on godliness, the former on morality.
		-- Max Stirner
%
Why, liberty of the press is only permission of the press, and the state never will or can voluntarily permit me to grind it to nothingness by the press.
		-- Max Stirner
%
The "equality of right" is a phantom just because right is nothing more and nothing less than admission, a matter of grace.
		-- Max Stirner
%
I am unique. My flesh is not their flesh, my mind is not their mind.
		-- Max Stirner
%
That the communist sees in you the man, the brother, is only the Sunday side of communism... If you were a "lazy-bones," he would not indeed fail to recognize the man in you, but would endeavour to cleanse him as a "lazy man" from laziness and to convert you to the faith that labor is man's "destiny and calling."
		-- Max Stirner
%
The republic is nothing whatever but absolute monarchy; for it makes no difference whether the monarch is called prince or people, both being a "majesty."
		-- Max Stirner
%
In a republic, all are lords, that is, all despotize one over another.
		-- Max Stirner
%
Liberate yourself as far as you can, and you have done your part; for it is not given to every one to break through all limits, or, more expressively: not to every one is that a limit which is a limit for the rest... he who overturns one of his limits may have shown others the way and the means.
		-- Max Stirner
%
Protestantism has actually put a man in the position of a country governed by secret police. The spy and eavesdropper, "conscience," watches over every motion of the mind, and all thought and action is for it a "matter of conscience," that is, police business.
		-- Max Stirner
%
Before the sacred, people lose all sense of power and all confidence; they occupy a powerless and humble attitude toward it. And yet no thing is sacred of itself, but by my declaring it sacred, by my declaration, my judgment, my bending the knee; in short, by my conscience.
		-- Max Stirner
%
The men of the future will yet fight their way to many a liberty that we do not even miss.
		-- Max Stirner
%
Man, your head is haunted; you have wheels in your head! You imagine great things, and depict to yourself a whole world of gods that has an existence for you, a spirit-realm to which you suppose yourself to be called, an ideal that beckons to you. You have a fixed idea!
		-- Max Stirner
%
A fist full of might goes farther than a bag full of rights.
		-- Max Stirner
%
